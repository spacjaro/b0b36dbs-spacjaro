-- 1. Zavolání a použití transakce
START TRANSACTION ISOLATION LEVEL SERIALIZABLE;

-- Vytvoříme záznam uživatele
INSERT INTO uživatel (email, rodné_číslo, heslo, uživatel_typ, telefonní_číslo, jméno, příjmení)
VALUES ('ukazka@transakce.com', '1234567890', 'sifrovane', 'host', '420-123-456-789', 'Ukazka', 'Transakce') 
RETURNING id INTO uživatel_id_var;

-- Vytvoříme uživateli jeho profil
INSERT INTO uživatelský_profil (uživatelské_jméno, profilový_obrázek_uri, uživatel_uživatel_id)
VALUES ('ukazkatransakce', 'https://www.gravatar.com/avatar/ukazkatransakce', uživatel_id_var);

-- Vytvoříme záznam hosta
INSERT INTO host (preference, uživatel_uživatel_id)
VALUES ('Pláž', uživatel_id_var);

COMMIT;

-- 1.2. Jaké konflikty by mohly nastat bez použití transakce (aneb proč používat transakce)?
-- a) Konzistence dat: Pokud dojde k chybě po vložení uživatele, ale před vložením profilu, skončíme s uživatelem bez odpovídajícího profilu v databázi. Tím by byla porušena konzistence dat.
-- b) Atomicita: Transakce zajišťují, že všechny operace v rámci transakčního bloku jsou považovány za jednu atomickou jednotku. To znamená, že buď se provedou všechny operace, nebo žádná. Bez transakce by některé operace mohly být úspěšné, zatímco jiné by selhaly, což by vedlo k nekonzistentnímu stavu dat v databázi.

/* - - - - - - - - - - - - - - - - - - - -  */

-- 2. Vytvoření a použití pohledu
-- 2.1. Vytvoření pohledu
CREATE VIEW host_details AS
SELECT uživatel.uživatel_id, uživatel.email, uživatel.rodné_číslo, uživatel.telefonní_číslo, uživatel.jméno, uživatel.příjmení, host.preference
FROM uživatel
INNER JOIN host ON uživatel.uživatel_id = host.uživatel_uživatel_id;

-- 2.2. Použití pohledu
SELECT * FROM host_details;

/* - - - - - - - - - - - - - - - - - - - -  */

-- 3. Vytvoření a použití trigerru
CREATE OR REPLACE FUNCTION host_before_insert()
RETURNS TRIGGER AS $$
BEGIN
    IF EXISTS (SELECT 1 FROM uživatel WHERE uživatel_id = NEW.uživatel_uživatel_id AND uživatel_typ = 'hostitel') THEN
        RAISE EXCEPTION 'Uživatel má typ "hostitel" a proto nemůže být přiřazen jako "host"';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER host_before_insert BEFORE INSERT ON host FOR EACH ROW
EXECUTE PROCEDURE host_before_insert();

/* - - - - - - - - - - - - - - - - - - - -  */

-- 4. Vytvoření a použití indexu
-- 4.1. Vytvoření indexu
CREATE INDEX idx_uživatel_email 
ON uživatel (email);

-- 4.2. Použití indexu (nepotřebujeme explicitně říkat, že máme použít index, protože ho už automaticky DBMS použije, pokud rozhodne, že to bude efektivnější)
SELECT * FROM uživatel WHERE email = 'johndoe@example.com';

-- 4.3. Proč použít index?
-- a) Rychlejší vyhledávání: Index ve sloupci e-mailu urychlí vyhledávání na základě e-mailu. To je výhodné zejména v případě, když máme velký počet uživatelů a často potřebujeme vyhledávat uživatele podle jejich e-mailu. (např. při přihlašování uživatele, při zobrazení profilu uživatele, při zobrazení informacích o uživateli v rezervaci atd.)
-- b) Efektivní třídění: Pokud potřebujeme často třídit uživatele na základě jejich e-mailu, může index tuto operaci zefektivnit.