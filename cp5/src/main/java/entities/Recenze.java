package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import org.hibernate.annotations.ColumnDefault;

import java.time.LocalDate;

@Entity
@Table(name = "recenze")
public class Recenze {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recenze_id_generator")
    @SequenceGenerator(name = "recenze_id_generator", sequenceName = "recenze_recenze_id_seq", allocationSize = 1)
    @Column(name = "recenze_id", nullable = false)
    private Integer id;

    @Column(name = "\"komentář\"", length = Integer.MAX_VALUE)
    private String komentář;

    @Column(name = "\"hodnocení\"")
    private Integer hodnocení;

    @Column(name = "datum")
    private LocalDate datum;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKomentář() {
        return komentář;
    }

    public void setKomentář(String komentář) {
        this.komentář = komentář;
    }

    public Integer getHodnocení() {
        return hodnocení;
    }

    public void setHodnocení(Integer hodnocení) {
        this.hodnocení = hodnocení;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}