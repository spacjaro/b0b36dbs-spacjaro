package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import org.hibernate.Hibernate;

import java.util.Objects;

@Embeddable
public class UzivatelHasRecenzeId implements java.io.Serializable {
    private static final long serialVersionUID = -760889317314218952L;
    @Column(name = "\"uživatel_uživatel_id\"", nullable = false)
    private Integer uživatelUživatelId;

    @Column(name = "recenze_recenze_id", nullable = false)
    private Integer recenzeRecenzeId;

    public Integer getUživatelUživatelId() {
        return uživatelUživatelId;
    }

    public void setUživatelUživatelId(Integer uživatelUživatelId) {
        this.uživatelUživatelId = uživatelUživatelId;
    }

    public Integer getRecenzeRecenzeId() {
        return recenzeRecenzeId;
    }

    public void setRecenzeRecenzeId(Integer recenzeRecenzeId) {
        this.recenzeRecenzeId = recenzeRecenzeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UzivatelHasRecenzeId entity = (UzivatelHasRecenzeId) o;
        return Objects.equals(this.uživatelUživatelId, entity.uživatelUživatelId) &&
                Objects.equals(this.recenzeRecenzeId, entity.recenzeRecenzeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uživatelUživatelId, recenzeRecenzeId);
    }

}