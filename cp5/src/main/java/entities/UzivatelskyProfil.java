package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "\"uživatelský_profil\"")
public class UzivatelskyProfil {
    @Id
    @ColumnDefault("nextval('uživatelský_profil_uživatelský_profil_id_seq'::regclass)")
    @Column(name = "\"uživatelský_profil_id\"", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "\"uživatelské_jméno\"", nullable = false)
    private String uživatelskéJméno;

    @Column(name = "\"profilový_obrázek_uri\"", length = 500)
    private String profilovýObrázekUri;

    @Column(name = "\"uživatel_uživatel_id\"", nullable = false)
    private Integer uživatelUživatelId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUživatelskéJméno() {
        return uživatelskéJméno;
    }

    public void setUživatelskéJméno(String uživatelskéJméno) {
        this.uživatelskéJméno = uživatelskéJméno;
    }

    public String getProfilovýObrázekUri() {
        return profilovýObrázekUri;
    }

    public void setProfilovýObrázekUri(String profilovýObrázekUri) {
        this.profilovýObrázekUri = profilovýObrázekUri;
    }

    public Integer getUživatelUživatelId() {
        return uživatelUživatelId;
    }

    public void setUživatelUživatelId(Integer uživatelUživatelId) {
        this.uživatelUživatelId = uživatelUživatelId;
    }

}