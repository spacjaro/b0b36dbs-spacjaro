package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.math.BigDecimal;

@Entity
@Table(name = "nemovitost")
public class Nemovitost {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nemovitost_id_generator")
    @SequenceGenerator(name = "nemovitost_id_generator", sequenceName = "nemovitost_nemovitost_id_seq", allocationSize = 1)
    @Column(name = "nemovitost_id", nullable = false)
    private Integer id;

    @Column(name = "\"název\"", nullable = false)
    private String název;

    @Column(name = "\"země\"", nullable = false)
    private String země;

    @Column(name = "\"město\"", nullable = false)
    private String město;

    @Column(name = "ulice", nullable = false)
    private String ulice;

    @Column(name = "\"psč\"", nullable = false)
    private String psč;

    @Column(name = "popis", length = Integer.MAX_VALUE)
    private String popis;

    @Column(name = "kapacita")
    private Integer kapacita;

    @Column(name = "cena", precision = 10, scale = 2)
    private BigDecimal cena;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "hostitel_hostitel_id", nullable = false)
    private Hostitel hostitelHostitel;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNázev() {
        return název;
    }

    public void setNázev(String název) {
        this.název = název;
    }

    public String getZemě() {
        return země;
    }

    public void setZemě(String země) {
        this.země = země;
    }

    public String getMěsto() {
        return město;
    }

    public void setMěsto(String město) {
        this.město = město;
    }

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice = ulice;
    }

    public String getPsč() {
        return psč;
    }

    public void setPsč(String psč) {
        this.psč = psč;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    public Integer getKapacita() {
        return kapacita;
    }

    public void setKapacita(Integer kapacita) {
        this.kapacita = kapacita;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public Hostitel getHostitelHostitel() {
        return hostitelHostitel;
    }

    public void setHostitelHostitel(Hostitel hostitelHostitel) {
        this.hostitelHostitel = hostitelHostitel;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}