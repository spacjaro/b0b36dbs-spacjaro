package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "\"uživatel_has_recenze\"")
public class UzivatelHasRecenze {
    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UzivatelHasRecenzeId id;

    @MapsId("uživatelUživatelId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "\"uživatel_uživatel_id\"", nullable = false)
    private Uzivatel uživatelUživatel;

    @MapsId("recenzeRecenzeId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "recenze_recenze_id", nullable = false)
    private Recenze recenzeRecenze;

    public UzivatelHasRecenzeId getId() {
        return id;
    }

    public void setId(UzivatelHasRecenzeId id) {
        this.id = id;
    }

    public Uzivatel getUživatelUživatel() {
        return uživatelUživatel;
    }

    public void setUživatelUživatel(Uzivatel uživatelUživatel) {
        this.uživatelUživatel = uživatelUživatel;
    }

    public Recenze getRecenzeRecenze() {
        return recenzeRecenze;
    }

    public void setRecenzeRecenze(Recenze recenzeRecenze) {
        this.recenzeRecenze = recenzeRecenze;
    }

}