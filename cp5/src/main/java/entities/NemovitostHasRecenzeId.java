package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import org.hibernate.Hibernate;

import java.util.Objects;

@Embeddable
public class NemovitostHasRecenzeId implements java.io.Serializable {
    private static final long serialVersionUID = -3564419569264276377L;
    @Column(name = "nemovitost_nemovitost_id", nullable = false)
    private Integer nemovitostNemovitostId;

    @Column(name = "recenze_recenze_id", nullable = false)
    private Integer recenzeRecenzeId;

    public Integer getNemovitostNemovitostId() {
        return nemovitostNemovitostId;
    }

    public void setNemovitostNemovitostId(Integer nemovitostNemovitostId) {
        this.nemovitostNemovitostId = nemovitostNemovitostId;
    }

    public Integer getRecenzeRecenzeId() {
        return recenzeRecenzeId;
    }

    public void setRecenzeRecenzeId(Integer recenzeRecenzeId) {
        this.recenzeRecenzeId = recenzeRecenzeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        NemovitostHasRecenzeId entity = (NemovitostHasRecenzeId) o;
        return Objects.equals(this.recenzeRecenzeId, entity.recenzeRecenzeId) &&
                Objects.equals(this.nemovitostNemovitostId, entity.nemovitostNemovitostId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recenzeRecenzeId, nemovitostNemovitostId);
    }

}