package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "platba")
public class Platba {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "platba_id_generator")
    @SequenceGenerator(name = "platba_id_generator", sequenceName = "platba_platba_id_seq", allocationSize = 1)
    @Column(name = "platba_id", nullable = false)
    private Integer id;

    @Column(name = "datum", nullable = false)
    private LocalDate datum;

    @Column(name = "\"částka\"", precision = 10, scale = 2)
    private BigDecimal částka;

    @Column(name = "\"způsob\"")
    private String způsob;

    @Column(name = "stav")
    private String stav;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "rezervace_rezervace_id", nullable = false)
    private Rezervace rezervaceRezervace;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public BigDecimal getČástka() {
        return částka;
    }

    public void setČástka(BigDecimal částka) {
        this.částka = částka;
    }

    public String getZpůsob() {
        return způsob;
    }

    public void setZpůsob(String způsob) {
        this.způsob = způsob;
    }

    public String getStav() {
        return stav;
    }

    public void setStav(String stav) {
        this.stav = stav;
    }

    public Rezervace getRezervaceRezervace() {
        return rezervaceRezervace;
    }

    public void setRezervaceRezervace(Rezervace rezervaceRezervace) {
        this.rezervaceRezervace = rezervaceRezervace;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}