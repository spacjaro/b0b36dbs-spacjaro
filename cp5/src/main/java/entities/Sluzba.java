package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "\"služba\"")
public class Sluzba {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "služba_id_generator")
    @SequenceGenerator(name = "služba_id_generator", sequenceName = "služba_služba_id_seq", allocationSize = 1)
    @Column(name = "služba_id", nullable = false)
    private Integer id;

    @Column(name = "\"název\"", nullable = false)
    private String název;

    @Column(name = "popis", length = Integer.MAX_VALUE)
    private String popis;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNázev() {
        return název;
    }

    public void setNázev(String název) {
        this.název = název;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}