package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
// @PrimaryKeyJoinColumn(name = "\"uživatel_uživatel_id\"")
@Table(name = "host")
// public class Host extends Uživatel {
public class Host {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "host_id_generator")
    @SequenceGenerator(name = "host_id_generator", sequenceName = "host_host_id_seq", allocationSize = 1)
    @Column(name = "host_id", nullable = false)
    private Integer id;

    @Column(name = "preference", length = 500)
    private String preference;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "\"uživatel_uživatel_id\"", nullable = false)
    private Uzivatel uživatelUživatel;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public Uzivatel getUživatelUživatel() {
        return uživatelUživatel;
    }

    public void setUživatelUživatel(Uzivatel uživatelUživatel) {
        this.uživatelUživatel = uživatelUživatel;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}