package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import org.hibernate.Hibernate;

import java.util.Objects;

@Embeddable
public class PrateliSeId implements java.io.Serializable {
    private static final long serialVersionUID = -4385162969288849310L;
    @Column(name = "\"uživatel_id\"", nullable = false)
    private Integer uživatelId;

    @Column(name = "\"přítel_id\"", nullable = false)
    private Integer přítelId;

    public Integer getUživatelId() {
        return uživatelId;
    }

    public void setUživatelId(Integer uživatelId) {
        this.uživatelId = uživatelId;
    }

    public Integer getPřítelId() {
        return přítelId;
    }

    public void setPřítelId(Integer přítelId) {
        this.přítelId = přítelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        PrateliSeId entity = (PrateliSeId) o;
        return Objects.equals(this.uživatelId, entity.uživatelId) &&
                Objects.equals(this.přítelId, entity.přítelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uživatelId, přítelId);
    }

}