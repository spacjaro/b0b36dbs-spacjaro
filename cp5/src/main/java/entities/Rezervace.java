package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "rezervace")
public class Rezervace {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rezervace_id_generator")
    @SequenceGenerator(name = "rezervace_id_generator", sequenceName = "rezervace_rezervace_id_seq", allocationSize = 1)
    @Column(name = "rezervace_id", nullable = false)
    private Integer id;

    @Column(name = "\"datum_příjezdu\"")
    private LocalDate datumPříjezdu;

    @Column(name = "datum_odjezdu")
    private LocalDate datumOdjezdu;

    @Column(name = "\"čas_příjezdu\"")
    private LocalTime časPříjezdu;

    @Column(name = "\"čas_odjezdu\"")
    private LocalTime časOdjezdu;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "host_host_id", nullable = false)
    private Host hostHost;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "nemovitost_nemovitost_id", nullable = false)
    private Nemovitost nemovitostNemovitost;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDatumPříjezdu() {
        return datumPříjezdu;
    }

    public void setDatumPříjezdu(LocalDate datumPříjezdu) {
        this.datumPříjezdu = datumPříjezdu;
    }

    public LocalDate getDatumOdjezdu() {
        return datumOdjezdu;
    }

    public void setDatumOdjezdu(LocalDate datumOdjezdu) {
        this.datumOdjezdu = datumOdjezdu;
    }

    public LocalTime getČasPříjezdu() {
        return časPříjezdu;
    }

    public void setČasPříjezdu(LocalTime časPříjezdu) {
        this.časPříjezdu = časPříjezdu;
    }

    public LocalTime getČasOdjezdu() {
        return časOdjezdu;
    }

    public void setČasOdjezdu(LocalTime časOdjezdu) {
        this.časOdjezdu = časOdjezdu;
    }

    public Host getHostHost() {
        return hostHost;
    }

    public void setHostHost(Host hostHost) {
        this.hostHost = hostHost;
    }

    public Nemovitost getNemovitostNemovitost() {
        return nemovitostNemovitost;
    }

    public void setNemovitostNemovitost(Nemovitost nemovitostNemovitost) {
        this.nemovitostNemovitost = nemovitostNemovitost;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}