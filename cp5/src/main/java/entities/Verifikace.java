package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDate;

@Entity
@Table(name = "verifikace")
public class Verifikace {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "verifikace_id_generator")
    @SequenceGenerator(name = "verifikace_id_generator", sequenceName = "verifikace_verifikace_id_seq", allocationSize = 1)
    @Column(name = "verifikace_id", nullable = false)
    private Integer id;

    @Column(name = "datum")
    private LocalDate datum;

    @Column(name = "typ")
    private String typ;

    @Column(name = "status")
    private String status;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "\"uživatel_uživatel_id\"", nullable = false)
    private Uzivatel uživatelUživatel;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Uzivatel getUživatelUživatel() {
        return uživatelUživatel;
    }

    public void setUživatelUživatel(Uzivatel uživatelUživatel) {
        this.uživatelUživatel = uživatelUživatel;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}