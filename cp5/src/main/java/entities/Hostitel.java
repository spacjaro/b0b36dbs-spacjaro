package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
// @PrimaryKeyJoinColumn(name = "\"uživatel_uživatel_id\"")
@Table(name = "hostitel")
// public class Hostitel extends Uživatel {
public class Hostitel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hostitel_id_generator")
    @SequenceGenerator(name = "hostitel_id_generator", sequenceName = "hostitel_hostitel_id_seq", allocationSize = 1)
    @Column(name = "hostitel_id", nullable = false)
    private Integer id;

    @Column(name = "superhostitel")
    private Boolean superhostitel;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "\"uživatel_uživatel_id\"", nullable = false)
    private Uzivatel uživatelUživatel;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSuperhostitel() {
        return superhostitel;
    }

    public void setSuperhostitel(Boolean superhostitel) {
        this.superhostitel = superhostitel;
    }

    public Uzivatel getUživatelUživatel() {
        return uživatelUživatel;
    }

    public void setUživatelUživatel(Uzivatel uživatelUživatel) {
        this.uživatelUživatel = uživatelUživatel;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}