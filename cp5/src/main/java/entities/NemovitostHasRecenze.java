package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "nemovitost_has_recenze")
public class NemovitostHasRecenze {
    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.AUTO)
    private NemovitostHasRecenzeId id;

    @MapsId("nemovitostNemovitostId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "nemovitost_nemovitost_id", nullable = false)
    private Nemovitost nemovitostNemovitost;

    @MapsId("recenzeRecenzeId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "recenze_recenze_id", nullable = false)
    private Recenze recenzeRecenze;

    public NemovitostHasRecenzeId getId() {
        return id;
    }

    public void setId(NemovitostHasRecenzeId id) {
        this.id = id;
    }

    public Nemovitost getNemovitostNemovitost() {
        return nemovitostNemovitost;
    }

    public void setNemovitostNemovitost(Nemovitost nemovitostNemovitost) {
        this.nemovitostNemovitost = nemovitostNemovitost;
    }

    public Recenze getRecenzeRecenze() {
        return recenzeRecenze;
    }

    public void setRecenzeRecenze(Recenze recenzeRecenze) {
        this.recenzeRecenze = recenzeRecenze;
    }

}