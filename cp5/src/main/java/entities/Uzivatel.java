package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import org.hibernate.annotations.ColumnDefault;

@Entity
// @Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "\"uživatel\"")
public class Uzivatel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "uživatel_id_generator")
    @SequenceGenerator(name = "uživatel_id_generator", sequenceName = "uživatel_uživatel_id_seq", allocationSize = 1)
    @Column(name = "\"uživatel_id\"", nullable = false)
    private Integer id;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "\"rodné_číslo\"", nullable = false)
    private String rodnéČíslo;

    @Column(name = "heslo", nullable = false, length = 500)
    private String heslo;

    @Column(name = "\"uživatel_typ\"", nullable = false)
    private String uživatelTyp;

    @Column(name = "\"telefonní_číslo\"")
    private String telefonníČíslo;

    @Column(name = "\"jméno\"")
    private String jméno;

    @Column(name = "\"příjmení\"")
    private String příjmení;

    @Column(name = "\"uživatelské_jméno\"", nullable = false)
    private String uživatelskéJméno;

    @Column(name = "\"profilový_obrázek_uri\"", length = 500)
    private String profilovýObrázekUri;

    @Column(name = "biografie", length = Integer.MAX_VALUE)
    private String biografie;

    @ColumnDefault("false")
    @Column(name = "deleted", nullable = false)
    private Boolean deleted = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRodnéČíslo() {
        return rodnéČíslo;
    }

    public void setRodnéČíslo(String rodnéČíslo) {
        this.rodnéČíslo = rodnéČíslo;
    }

    public String getHeslo() {
        return heslo;
    }

    public void setHeslo(String heslo) {
        this.heslo = heslo;
    }

    public String getUživatelTyp() {
        return uživatelTyp;
    }

    public void setUživatelTyp(String uživatelTyp) {
        this.uživatelTyp = uživatelTyp;
    }

    public String getTelefonníČíslo() {
        return telefonníČíslo;
    }

    public void setTelefonníČíslo(String telefonníČíslo) {
        this.telefonníČíslo = telefonníČíslo;
    }

    public String getJméno() {
        return jméno;
    }

    public void setJméno(String jméno) {
        this.jméno = jméno;
    }

    public String getPříjmení() {
        return příjmení;
    }

    public void setPříjmení(String příjmení) {
        this.příjmení = příjmení;
    }

    public String getUživatelskéJméno() {
        return uživatelskéJméno;
    }

    public void setUživatelskéJméno(String uživatelskéJméno) {
        this.uživatelskéJméno = uživatelskéJméno;
    }

    public String getProfilovýObrázekUri() {
        return profilovýObrázekUri;
    }

    public void setProfilovýObrázekUri(String profilovýObrázekUri) {
        this.profilovýObrázekUri = profilovýObrázekUri;
    }

    public String getBiografie() {
        return biografie;
    }

    public void setBiografie(String biografie) {
        this.biografie = biografie;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}