package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "\"nemovitost_has_služba\"")
public class NemovitostHasSluzba {
    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.AUTO)
    private NemovitostHasSluzbaId id;

    @MapsId("nemovitostNemovitostId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "nemovitost_nemovitost_id", nullable = false)
    private Nemovitost nemovitostNemovitost;

    @MapsId("službaSlužbaId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "\"služba_služba_id\"", nullable = false)
    private Sluzba službaSlužba;

    public NemovitostHasSluzbaId getId() {
        return id;
    }

    public void setId(NemovitostHasSluzbaId id) {
        this.id = id;
    }

    public Nemovitost getNemovitostNemovitost() {
        return nemovitostNemovitost;
    }

    public void setNemovitostNemovitost(Nemovitost nemovitostNemovitost) {
        this.nemovitostNemovitost = nemovitostNemovitost;
    }

    public Sluzba getSlužbaSlužba() {
        return službaSlužba;
    }

    public void setSlužbaSlužba(Sluzba službaSlužba) {
        this.službaSlužba = službaSlužba;
    }

}