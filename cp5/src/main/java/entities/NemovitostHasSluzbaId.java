package entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import org.hibernate.Hibernate;

import java.util.Objects;

@Embeddable
public class NemovitostHasSluzbaId implements java.io.Serializable {
    private static final long serialVersionUID = 7917143439323267015L;
    @Column(name = "nemovitost_nemovitost_id", nullable = false)
    private Integer nemovitostNemovitostId;

    @Column(name = "\"služba_služba_id\"", nullable = false)
    private Integer službaSlužbaId;

    public Integer getNemovitostNemovitostId() {
        return nemovitostNemovitostId;
    }

    public void setNemovitostNemovitostId(Integer nemovitostNemovitostId) {
        this.nemovitostNemovitostId = nemovitostNemovitostId;
    }

    public Integer getSlužbaSlužbaId() {
        return službaSlužbaId;
    }

    public void setSlužbaSlužbaId(Integer službaSlužbaId) {
        this.službaSlužbaId = službaSlužbaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        NemovitostHasSluzbaId entity = (NemovitostHasSluzbaId) o;
        return Objects.equals(this.službaSlužbaId, entity.službaSlužbaId) &&
                Objects.equals(this.nemovitostNemovitostId, entity.nemovitostNemovitostId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(službaSlužbaId, nemovitostNemovitostId);
    }

}