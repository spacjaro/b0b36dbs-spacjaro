package entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "\"přátelí_se\"")
public class PrateliSe {
    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.AUTO)
    private PrateliSeId id;

    @MapsId("uživatelId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "\"uživatel_id\"", nullable = false)
    private Uzivatel uživatel;

    @MapsId("přítelId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "\"přítel_id\"", nullable = false)
    private Uzivatel přítel;

    public PrateliSeId getId() {
        return id;
    }

    public void setId(PrateliSeId id) {
        this.id = id;
    }

    public Uzivatel getUživatel() {
        return uživatel;
    }

    public void setUživatel(Uzivatel uživatel) {
        this.uživatel = uživatel;
    }

    public Uzivatel getPřítel() {
        return přítel;
    }

    public void setPřítel(Uzivatel přítel) {
        this.přítel = přítel;
    }

}