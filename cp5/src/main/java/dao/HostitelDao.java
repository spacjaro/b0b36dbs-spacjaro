package dao;

import entities.Hostitel;
import jakarta.persistence.EntityManager;

public class HostitelDao extends BaseDao<Hostitel> {
    public HostitelDao(EntityManager em) {
        super(Hostitel.class, em);
    }

    // public Hostitel createHost(Hostitel host) throws RuntimeException {
    //     try {
    //         Hostitel result = persist(host);
    //         return result;
    //     } catch (Exception e) {
    //         throw new RuntimeException("(!) Create host error: " + e.getMessage(), e);  
    //     }
    // }
}
