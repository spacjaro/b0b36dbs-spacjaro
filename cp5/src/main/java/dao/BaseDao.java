package dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.PersistenceException;
import jakarta.persistence.TypedQuery;

import java.util.List;

public class BaseDao<GenericType> {
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("ApplicationPU");
    protected EntityManager em = emf.createEntityManager();
    
    protected final Class<GenericType> type;

    public BaseDao(Class<GenericType> type, EntityManager em) {
        this.type = type;
        this.em = em;
    }

    protected EntityManager getEntityManager() {
        // Safer for multithreading
        return emf.createEntityManager();
    }


    /**
     * Finds an entity of type GenericType by its ID.
     *
     * @param id the ID of the entity to find
     * @return the found entity, or null if not found
     * @throws IllegalArgumentException if the ID is null
     */
    public GenericType find(Long id) throws PersistenceException {
        if (id == null) {
            throw new IllegalArgumentException("ID cannot be null!");
        }

        try {
            EntityManager em = getEntityManager();
            GenericType result = em.find(type, id);
            return result;
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }


    /**
     * Retrieves all entities of the specified generic type from the database.
     *
     * @return a list of entities of the specified generic type
     * @throws PersistenceException if an error occurs while accessing the database
     */
    public List<GenericType> findAll() throws PersistenceException {
        try {
            EntityManager em = getEntityManager();
            List<GenericType> result = em.createQuery("SELECT e FROM " + type.getSimpleName() + " e", type).getResultList();
            return result;
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }


    /**
     * Finds entities of type GenericType based on a specified attribute.
     *
     * @param attributeName the name of the attribute to search by
     * @param value the value of the attribute to search for
     * @return a list of found entities, or an empty list if none are found
     * @throws IllegalArgumentException if the attributeName or value is null
     * @throws PersistenceException if any runtime exception occurs during the persist operation
     */
    public List<GenericType> findByAttribute(String attributeName, Object value) throws IllegalArgumentException, PersistenceException {
        if (attributeName == null || value == null) {
            throw new IllegalArgumentException("Attribute name and value cannot be null!");
        }

        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<GenericType> query = em.createQuery("SELECT e FROM " + type.getSimpleName() + " e WHERE e." + attributeName + " = :value", type);
            query.setParameter("value", value);
            return query.getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }


    /**
     * Persists (saves) a new entity in the database.
     *
     * @param entity the entity to be persisted (cannot be null)
     * @return the persisted (create) entity
     * @throws IllegalArgumentException if the provided entity is null
     * @throws PersistenceException if any runtime exception occurs during the persist operation
     */
    public GenericType persist(GenericType entity) throws IllegalArgumentException, PersistenceException {
        if (entity == null) {
            throw new IllegalArgumentException("Entity cannot be null!");
        }

        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
            return entity;
        } catch (RuntimeException e) {
            if (em != null && em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            throw new PersistenceException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }


    /**
     * Updates an entity in the database.
     *
     * @param entity the entity to be updated (cannot be null)
     * @return the updated entity
     * @throws IllegalArgumentException if the provided entity is null
     * @throws PersistenceException if any runtime exception occurs during the update operation
     */
    public GenericType update(GenericType entity) throws IllegalArgumentException, PersistenceException {
        if (entity == null) {
            throw new IllegalArgumentException("Entity cannot be null!");
        }

        try {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            GenericType updatedEntity = em.merge(entity);
            em.getTransaction().commit();
            return updatedEntity;
        } catch (RuntimeException e) {
            if (em != null && em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            throw new PersistenceException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }


    /**
     * Removes the specified entity from the database.
     *
     * @param entity the entity to be removed
     * @throws IllegalArgumentException if the entity is null
     * @throws PersistenceException if an error occurs during the persistence operation
     */
    public void remove(GenericType entity) throws IllegalArgumentException, PersistenceException {
        if (entity == null) {
            throw new IllegalArgumentException("Entity cannot be null!");
        }

        try {
            EntityManager em = getEntityManager();

            em.getTransaction().begin();
            GenericType entityToBeRemoved = em.merge(entity);
            em.remove(entityToBeRemoved);
            em.getTransaction().commit();
        } catch (RuntimeException e) {
            if (em != null && em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            throw new PersistenceException(e);
        } finally {
            if(em != null) {
                em.close();
            }
        }
    }
}