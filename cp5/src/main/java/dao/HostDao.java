package dao;

import entities.Host;
import jakarta.persistence.EntityManager;

public class HostDao extends BaseDao<Host> {
    public HostDao(EntityManager em) {
        super(Host.class, em);
    }

    // public Host createGuest(Host guest) throws RuntimeException {
    //     try {
    //         Host result = persist(guest);
    //         return result;
    //     } catch (Exception e) {
    //         throw new RuntimeException("(!) Create guest error: " + e.getMessage(), e);  
    //     }
    // }
}
