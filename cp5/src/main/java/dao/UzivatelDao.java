package dao;

import entities.Uzivatel;
import jakarta.persistence.EntityManager;

import java.util.List;

public class UzivatelDao extends BaseDao<Uzivatel> {
    public UzivatelDao(EntityManager em) {
        super(Uzivatel.class, em);
    }

    public List<Uzivatel> getAllUsers() throws RuntimeException {
        try {
            return findAll();
        } catch (Exception e) {
            throw new RuntimeException("(!) Get all users error: " + e.getMessage(), e);
        }
    }

    public Uzivatel getUserByEmail(String email) throws RuntimeException {
        try {
            List<Uzivatel> result = findByAttribute("email", email);
            
            if(result.isEmpty()) {
                return null;
            }

            return result.get(0);
        } catch (Exception e) {
            throw new RuntimeException("(!) Get user by email error: " + e.getMessage(), e);
        }
    }

    // public Uživatel createUser(Uživatel user) throws RuntimeException {
    //     try {
    //         Uživatel result = persist(user);
    //         return result;
    //     } catch (Exception e) {
    //         throw new RuntimeException("(!) Create user error: " + e.getMessage(), e);  
    //     }
    // }

    public Uzivatel updateUser(Uzivatel user) throws RuntimeException {
        try {
            Uzivatel updatedUser = update(user);
            return updatedUser;
        } catch (Exception e) {
            throw new RuntimeException("(!) Update user error: " + e.getMessage(), e);
        }
    }

    public void deleteUser(Uzivatel user) throws RuntimeException {
        try {
            remove(user);
        } catch (Exception e) {
            throw new RuntimeException("(!) Delete user error: " + e.getMessage(), e);
        }
    }
}
