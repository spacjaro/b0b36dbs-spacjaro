import entities.Host;
import entities.Hostitel;
import entities.Uzivatel;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

import dao.UzivatelDao;
// import dao.HostDao;
// import dao.HostitelDao;

public class Main {
    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ApplicationPU");
        EntityManager em = emf.createEntityManager();


        // (1.) Get all users
        UzivatelDao userDao = new UzivatelDao(em);

        try {
            List<Uzivatel> allUsers = userDao.getAllUsers();
        
            if(allUsers != null) {
                for (Uzivatel uživatel : allUsers) {
                    System.out.println(uživatel.getEmail());
                }
            } else {
                System.err.println("User not found!");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        printDivider();
        


        // (2.) Create a user (transaction)
        String userEmail = "johndoe@example.com";

        try {

            em = emf.createEntityManager();
        
            em.getTransaction().begin();
    
            Uzivatel newUser = new Uzivatel();
            newUser.setEmail(userEmail);
            newUser.setRodnéČíslo("1234567890");
            newUser.setHeslo("password123");
            newUser.setUživatelTyp("host");
            newUser.setTelefonníČíslo("1234567890");
            newUser.setJméno("John");
            newUser.setPříjmení("Doe");
            newUser.setUživatelskéJméno("johndoe");
            newUser.setProfilovýObrázekUri("http://example.com/profile.jpg");
            newUser.setBiografie("This is a biography.");
            
            // em.getTransaction().commit();
            try {
                em.persist(newUser);
                System.out.println("User was created!");
            } catch (Exception e) {
                throw new RuntimeException("(!) Create user error: " + e.getMessage(), e);  
            }
    
            if(newUser.getUživatelTyp() == "host") {
                try {
                    Host guest = new Host();

                    guest.setPreference("Moře");
                    guest.setUživatelUživatel(newUser);
        
                    // HostDao guestDao = new HostDao(em);

                    em.persist(guest);
                    System.out.println("Guest was created!");
                } catch (Exception e) {
                    throw new RuntimeException("(!) Create user error: " + e.getMessage(), e);  
                }
            } else if (newUser.getUživatelTyp() == "hostitel") {
                try {
                    Hostitel host = new Hostitel();

                    host.setSuperhostitel(true);
                    host.setUživatelUživatel(newUser);
        
                    // HostitelDao hostDao = new HostitelDao(em);

                    em.persist(host);
                    System.out.println("Host was created!");
                } catch (Exception e) {
                    throw new RuntimeException("(!) Create user error: " + e.getMessage(), e);  
                }
            } else {
                throw new RuntimeException("(!) Create host/guest error!");  
            }
    
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("(!) Transaction rollback");
            em.getTransaction().rollback();
        }

        printDivider();


        
        // (3.) Get user by email (parameterized query)
        Uzivatel userByEmail = null;

        try {
            userByEmail = userDao.getUserByEmail(userEmail);
            
            if(userByEmail != null) {
                System.out.println("User with email address " + userEmail + " :" + userByEmail.getEmail());
            } else {
                System.err.println("User was not found!");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        printDivider();



        // (4.) Update user
        Uzivatel updatedUserByEmail = null;

        try {
            if(userByEmail != null) {
                userByEmail.setBiografie("Updated biography");
            }

            userDao.updateUser(userByEmail);
        
            updatedUserByEmail = userDao.getUserByEmail(userEmail);

            if(updatedUserByEmail != null) {
                System.out.println("Upadted user with email address " + userEmail + " :" + updatedUserByEmail.getBiografie());
            } else {
                System.err.println("User was not found!");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        printDivider();



        // (5.) Delete user
        try {
            userDao.deleteUser(updatedUserByEmail);

            Uzivatel deletedUser = userDao.getUserByEmail(userEmail);

            if(deletedUser == null) {
                System.out.println("User was deleted!");
            } else {
                System.err.println("User was not deleted!");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void printDivider() {
        System.out.println("--------------------");
    }
}
