-- Vnější spojení tabulek
SELECT u.jméno, u.příjmení, h.preference 
FROM uživatel u
LEFT JOIN host h ON u.uživatel_id = h.uživatel_uživatel_id;


-- Vnitřní spojení tabulek
SELECT u.jméno, u.příjmení, h.preference
FROM uživatel u
INNER JOIN host h ON u.uživatel_id = h.uživatel_uživatel_id;


-- Podmínka na data
SELECT * FROM nemovitost 
WHERE cena > 1000;


-- Agregaci a podmínku na hodnotu agregační funkce
SELECT nemovitost.nemovitost_id, nemovitost.název, AVG(recenze.hodnocení) AS průměrné_hodnocení
FROM nemovitost
JOIN nemovitost_has_recenze ON nemovitost.nemovitost_id = nemovitost_has_recenze.nemovitost_nemovitost_id
JOIN recenze ON recenze.recenze_id = nemovitost_has_recenze.recenze_recenze_id
GROUP BY nemovitost.nemovitost_id, nemovitost.název;


-- Řazení a stránkování
SELECT * FROM nemovitost 
ORDER BY cena DESC 
LIMIT 3 OFFSET 1;


-- Množinové operace
SELECT u.jméno, u.příjmení
FROM uživatel u
WHERE u.uživatel_id NOT IN (
    SELECT uživatel_uživatel_id
    FROM host
);


-- Vnořený SELECT
SELECT u.jméno, u.příjmení, ( 
    SELECT COUNT(*) 
    FROM nemovitost n 
    JOIN hostitel h ON h.uživatel_uživatel_id = u.uživatel_id 
    WHERE n.hostitel_hostitel_id = h.hostitel_id
) AS počet_nemovitostí FROM uživatel u;