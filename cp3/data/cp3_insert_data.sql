INSERT INTO uživatel (email, rodné_číslo, heslo, uživatel_typ, telefonní_číslo, jméno, příjmení, uživatelské_jméno, profilový_obrázek_uri, biografie) VALUES 
    ('Suzanne65@hotmail.com', '9900006347', 'rZzIy2B6wRiRnGg', 'hostitel', '420-294-871-843', 'Suzanne', 'Williamson', 'Suzanne65', 'https://www.gravatar.com/avatar/0d1', 'Short bio for Suzanne'),
    ('Dillan_Wyman@gmail.com', '4138947531', '8UdQNWwLkaYgyed', 'host', '420-577-336-196', 'Dillan', 'Wyman', 'Dillan_Wyman', 'https://www.gravatar.com/avatar/0d2', 'Short bio for Dillan'),
    ('Boris.Reichert@gmail.com', '8119811036', 'bgu7hBBzkRBbbhk', 'host', '420-540-275-716', 'Boris', 'Reichert', 'Boris.Reichert', 'https://www.gravatar.com/avatar/0d3', 'Short bio for Boris'),
    ('Domenica@hotmail.com', '7795344067', 's_gfVFCurZkUSJr', 'host', '420-270-268-520', 'Domenica', 'Leuschke', 'Domenica', 'https://www.gravatar.com/avatar/0d4', 'Short bio for Domenica'),
    ('Clair_Hayes@hotmail.com', '2266796698', 'SsHktg1RFTzdA4R', 'hostitel', '420-334-570-033', 'Clair', 'Hayes', 'Clair_Hayes', 'https://www.gravatar.com/avatar/0d5', 'Short bio for Clair'),
    ('Connie@hotmail.com', '3372326807', 'myCOtmQ7_nDFfgS', 'host', '420-143-226-942', 'Connie', 'Koss', 'Connie', 'https://www.gravatar.com/avatar/0d6', 'Short bio for Connie'),
    ('Lawsone14@hotmail.com', '2950641149', 'QXmO0x3xC2oDq_C', 'host', '420-109-006-859', 'Lawson', 'Boehm', 'Lawsone14', 'https://www.gravatar.com/avatar/0d7', 'Short bio for Lawson'),
    ('pepe66@yahoo.com', '7850818698', 'bfgrj6Vr84CtLEX', 'host', '420-307-823-009', 'Adolf', 'Hudson', 'pepe66', 'https://www.gravatar.com/avatar/0d8', 'Short bio for Adolf'),
    ('Maye21@gmail.com', '9494266209', 'LkHD_yqS7S1TUUr', 'host', '420-729-908-983', 'Maye', 'Von', 'Maye21', 'https://www.gravatar.com/avatar/0d9', 'Short bio for Maye'),
    ('frankhotmail.com', '7734540671', 's_gfVFCurZkUSJr', 'host', '420-321-268-020', 'Frank', 'Hold', 'frank', 'https://www.gravatar.com/avatar/0d10', 'Short bio for Frank');

INSERT INTO přátelí_se (uživatel_id, přítel_id) VALUES 
    (1,2), (1,4),
    (3, 5), (3, 6),
    (5, 7), (5, 8),
    (7, 5), (7, 10),
    (9, 10);

INSERT INTO verifikace (datum, typ, status, uživatel_uživatel_id) VALUES 
    ('2020-01-01', 'email', 'ověřeno', 1 ),
    ('2020-01-01', 'telefon', 'ověřeno', 2 ),
    ('2020-01-01', 'email', 'ověřeno', 3 ),
    ('2020-01-01', NULL, 'neověřeno', 4 ),
    ('2020-01-01', NULL, 'neověřeno', 5 ),
    ('2020-01-01', 'telefon', 'ověřeno', 6 ),
    ('2020-01-01', NULL, 'neověřeno', 7 ),
    ('2020-01-01', NULL, 'neověřeno', 8 ),
    ('2020-01-01', 'email', 'ověřeno', 9 ),
    ('2020-01-01', NULL, 'neověřeno', 10 );

INSERT INTO recenze (komentář, hodnocení, datum) VALUES
    ('Krásné místo', 5, '2020-01-01'),
    ('Příjemný hostitel', 4, '2020-01-01'),
    ('Vše bylo v pořádku', 5, '2020-01-01'),
    ('Nemohu doporučit', 1, '2020-01-01'),
    ('Vše bylo v pořádku', 5, '2020-01-01'),
    ('Vtipný', 5, '2020-01-01'),
    ('Příjemný hostitel', 4, '2020-01-01'),
    ('Vše bylo v pořádku', 5, '2020-01-01'),
    ('Nemohu doporučit', 1, '2020-01-01'),
    ('Vše bylo v pořádku', 5, '2020-01-01');

INSERT INTO host (preference, uživatel_uživatel_id) VALUES
    ('Pláž', 2),
    ('Rušné město', 3),
    ('Tichý les', 4),
    ('Pláž', 6),
    ('Příroda', 7),
    ('Řeka', 8),
    ('Ostrov', 9),
    ('Velká postel', 10);

INSERT INTO hostitel (superhostitel, uživatel_uživatel_id) VALUES
    (TRUE, 1),
    (FALSE, 5);

INSERT INTO nemovitost (název, země, město, ulice, psč, popis, cena, kapacita, hostitel_hostitel_id) VALUES
    ('Útulný domeček u lesa', 'Česká republika', 'Praha', 'Ulice Lesní 123', '11000', 'Krásný domeček ideální pro odpočinek v přírodě.', 2500.00, 4, 1),
    ('Apartmán v centru', 'Česká republika', 'Brno', 'Náměstí Svobody 1', '60200', 'Moderní apartmán přímo v centru města.', 3500.00, 2, 1),
    ('Venkovská chalupa', 'Česká republika', 'Český Krumlov', 'Horní 56', '38101', 'Tradiční chalupa s krásným výhledem na okolní krajinu.', 1800.00, 6, 1),
    ('Luxusní vila u jezera', 'Česká republika', 'Lipno', 'Jezerní 12', '38278', 'Elegantní vila s vlastním přístupem k jezeru.', 5000.00, 8, 2),
    ('Apartmán v historickém domě', 'Česká republika', 'Olomouc', 'Horní náměstí 8', '77900', 'Stylový apartmán v renesanční budově.', 3200.00, 4, 2);


INSERT INTO rezervace (datum_příjezdu, datum_odjezdu, čas_příjezdu, čas_odjezdu, host_host_id, nemovitost_nemovitost_id) VALUES
    ('2024-05-10', '2024-05-15', '14:00', '10:00', 1, 1),
    ('2024-06-20', '2024-06-25', '15:00', '11:00', 2, 2),
    ('2024-07-05', '2024-07-10', '13:00', '09:00', 3, 3),
    ('2024-08-12', '2024-08-17', '12:00', '08:00', 4, 4),
    ('2024-09-03', '2024-09-08', '16:00', '12:00', 5, 5);

INSERT INTO platba (datum, částka, způsob, stav, rezervace_rezervace_id) VALUES
    ('2024-05-12', 1500.00, 'karta', 'zaplaceno', 1),
    ('2024-06-22', 2000.00, 'hotovost', 'zaplaceno', 2),
    ('2024-07-07', 1800.00, 'karta', 'zaplaceno', 3),
    ('2024-08-15', 2500.00, 'karta', 'zaplaceno', 4),
    ('2024-09-05', 3200.00, 'hotovost', 'zaplaceno', 5);

INSERT INTO služba (název, popis) VALUES
    ('Wi-Fi připojení', 'Rychlé a spolehlivé připojení k internetu.'),
    ('Snídaně', 'Každé ráno vám připravíme bohatou snídani.'),
    ('Úklid', 'Profesionální úklid každý den během vašeho pobytu.'),
    ('Parkování', 'Bezplatné parkování pro naše hosty.'),
    ('Masáže', 'Relaxační masáže přímo ve vašem apartmánu.');

INSERT INTO uživatel_has_recenze (uživatel_uživatel_id, recenze_recenze_id) VALUES
    (1, 5),
    (2, 6),
    (3, 7);

INSERT INTO nemovitost_has_recenze (nemovitost_nemovitost_id, recenze_recenze_id) VALUES
    (1, 1),
    (1, 2),
    (2, 3);

INSERT INTO nemovitost_has_služba (nemovitost_nemovitost_id, služba_služba_id) VALUES
    (1, 1),
    (1, 3),
    (2, 1),
    (2, 2),
    (2, 3),
    (2, 4);