-- Init
CREATE SCHEMA IF NOT EXISTS spacjaro DEFAULT CHARACTER SET utf8mb4;
SET SCHEMA spacjaro;



-- Delete tables and triggers before creating them
DROP TABLE IF EXISTS uživatel CASCADE;
DROP TABLE IF EXISTS přátelí_se CASCADE;
DROP TABLE IF EXISTS verifikace CASCADE;
DROP TABLE IF EXISTS recenze CASCADE;
DROP TABLE IF EXISTS host CASCADE;
DROP TABLE IF EXISTS hostitel CASCADE;
DROP TABLE IF EXISTS nemovitost CASCADE;
DROP TABLE IF EXISTS rezervace CASCADE;
DROP TABLE IF EXISTS platba CASCADE;
DROP TABLE IF EXISTS služba CASCADE;
DROP TABLE IF EXISTS uživatel_has_recenze CASCADE;
DROP TABLE IF EXISTS nemovitost_has_recenze CASCADE;
DROP TABLE IF EXISTS nemovitost_has_služba CASCADE;
DROP FUNCTION IF EXISTS host_before_insert;
DROP FUNCTION IF EXISTS host_before_update;
DROP FUNCTION IF EXISTS hostitel_before_insert;
DROP FUNCTION IF EXISTS hostitel_before_update;



-- Tables
CREATE TABLE IF NOT EXISTS uživatel (
  uživatel_id SERIAL PRIMARY KEY NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  rodné_číslo VARCHAR(255) NOT NULL UNIQUE,
  heslo VARCHAR(500) NOT NULL,
  uživatel_typ VARCHAR(255) CHECK (uživatel_typ IN ('host', 'hostitel')) NOT NULL,
  telefonní_číslo VARCHAR(255) NULL,
  jméno VARCHAR(255) NULL,
  příjmení VARCHAR(255) NULL,
  uživatelské_jméno VARCHAR(255) NOT NULL UNIQUE,
  profilový_obrázek_uri VARCHAR(500) NULL,
  biografie TEXT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE IF NOT EXISTS přátelí_se (
  uživatel_id INT NOT NULL,
  přítel_id INT NOT NULL,
  PRIMARY KEY (uživatel_id, přítel_id),
  CONSTRAINT fk_přátelí_se_uživatel
    FOREIGN KEY (uživatel_id)
    REFERENCES uživatel (uživatel_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_přátelí_se_přítel
    FOREIGN KEY (přítel_id)
    REFERENCES uživatel (uživatel_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS verifikace (
  verifikace_id SERIAL PRIMARY KEY NOT NULL,
  datum DATE NULL,
  typ VARCHAR(255) CHECK (status IN ('email', 'telefon', NULL)),
  status VARCHAR(255) CHECK (status IN ('neověřeno', 'ověřeno')),
  uživatel_uživatel_id INT NOT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT fk_verifikace_uživatel
    FOREIGN KEY (uživatel_uživatel_id)
    REFERENCES uživatel (uživatel_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS recenze (
  recenze_id SERIAL PRIMARY KEY NOT NULL,
  komentář TEXT NULL,
  hodnocení INT CHECK (hodnocení >= 0 AND hodnocení <= 5),
  datum DATE NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE IF NOT EXISTS host (
  host_id SERIAL PRIMARY KEY NOT NULL,
  preference VARCHAR(500) NULL,
  uživatel_uživatel_id INT NOT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT fk_host_uživatel
    FOREIGN KEY (uživatel_uživatel_id)
    REFERENCES uživatel (uživatel_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS hostitel (
  hostitel_id SERIAL PRIMARY KEY NOT NULL,
  superhostitel BOOLEAN NULL,
  uživatel_uživatel_id INT NOT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT fk_hostitel_uživatel
    FOREIGN KEY (uživatel_uživatel_id)
    REFERENCES uživatel (uživatel_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS nemovitost (
  nemovitost_id SERIAL PRIMARY KEY NOT NULL,
  název VARCHAR(255) NOT NULL,
  země VARCHAR(255) NOT NULL,
  město VARCHAR(255) NOT NULL,
  ulice VARCHAR(255) NOT NULL,
  psč VARCHAR(255) NOT NULL,
  popis TEXT NULL,
  kapacita INT NULL,
  cena DECIMAL(10,2) NULL,
  hostitel_hostitel_id INT NOT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT fk_nemovitost_hostitel
    FOREIGN KEY (hostitel_hostitel_id)
    REFERENCES hostitel (hostitel_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS rezervace (
  rezervace_id SERIAL PRIMARY KEY NOT NULL,
  datum_příjezdu DATE NULL,
  datum_odjezdu DATE NULL,
  čas_příjezdu TIME NULL,
  čas_odjezdu TIME NULL,
  host_host_id INT NOT NULL,
  nemovitost_nemovitost_id INT NOT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT fk_rezervace_host
    FOREIGN KEY (host_host_id)
    REFERENCES host (host_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_rezervace_nemovitost
    FOREIGN KEY (nemovitost_nemovitost_id)
    REFERENCES nemovitost (nemovitost_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS platba (
  platba_id SERIAL PRIMARY KEY NOT NULL,
  datum DATE NOT NULL,
  částka DECIMAL(10,2) NULL,
  způsob VARCHAR(255) CHECK (způsob IN ('hotovost', 'karta')),
  stav VARCHAR(255) CHECK (stav IN ('nezaplaceno', 'zaplaceno', 'vráceno')),
  rezervace_rezervace_id INT NOT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL,
  CONSTRAINT fk_platba_rezervace
    FOREIGN KEY (rezervace_rezervace_id)
    REFERENCES rezervace (rezervace_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS služba (
  služba_id SERIAL PRIMARY KEY NOT NULL,
  název VARCHAR(255) NOT NULL UNIQUE,
  popis TEXT NULL,
  deleted BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE IF NOT EXISTS uživatel_has_recenze (
  uživatel_uživatel_id INT NOT NULL,
  recenze_recenze_id INT NOT NULL,
  PRIMARY KEY (uživatel_uživatel_id, recenze_recenze_id),
  CONSTRAINT fk_uživatel_has_recenze_uživatel
    FOREIGN KEY (uživatel_uživatel_id)
    REFERENCES uživatel (uživatel_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_uživatel_has_recenze_recenze
    FOREIGN KEY (recenze_recenze_id)
    REFERENCES recenze (recenze_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS nemovitost_has_recenze (
  nemovitost_nemovitost_id INT NOT NULL,
  recenze_recenze_id INT NOT NULL,
  PRIMARY KEY (nemovitost_nemovitost_id, recenze_recenze_id),
  CONSTRAINT fk_nemovitost_has_recenze_nemovitost
    FOREIGN KEY (nemovitost_nemovitost_id)
    REFERENCES nemovitost (nemovitost_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_nemovitost_has_recenze_recenze
    FOREIGN KEY (recenze_recenze_id)
    REFERENCES recenze (recenze_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS nemovitost_has_služba (
  nemovitost_nemovitost_id INT NOT NULL, 
  služba_služba_id INT NOT NULL,
  PRIMARY KEY (nemovitost_nemovitost_id, služba_služba_id),
  CONSTRAINT fk_nemovitost_has_služba_nemovitost
    FOREIGN KEY (nemovitost_nemovitost_id)
    REFERENCES nemovitost (nemovitost_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_nemovitost_has_služba_služba
    FOREIGN KEY (služba_služba_id)
    REFERENCES služba (služba_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);



-- Triggers
CREATE OR REPLACE FUNCTION host_before_insert()
RETURNS TRIGGER AS $$
BEGIN
    -- IF EXISTS (SELECT 1 FROM User WHERE uživatel_id = host_id AND uživatel_typ = 'hostitel') THEN
    IF EXISTS (SELECT 1 FROM uživatel WHERE uživatel_id = NEW.uživatel_uživatel_id AND uživatel_typ = 'hostitel') THEN
        RAISE EXCEPTION 'Uživatel má typ "hostitel" a proto nemůže být přiřazen jako "host"';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER host_before_insert BEFORE INSERT ON host FOR EACH ROW
EXECUTE PROCEDURE host_before_insert();


CREATE OR REPLACE FUNCTION host_before_update()
RETURNS TRIGGER AS $$
BEGIN
    -- IF EXISTS (SELECT 1 FROM User WHERE uživatel_id = NEW.host_id AND uživatel_typ = 'hostitel') THEN
    IF EXISTS (SELECT 1 FROM uživatel WHERE uživatel_id = NEW.uživatel_uživatel_id AND uživatel_typ = 'hostitel') THEN
        RAISE EXCEPTION 'Uživatel má typ "hostitel" a proto nemůže být přiřazen jako "host"';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER host_before_update BEFORE UPDATE ON host FOR EACH ROW
EXECUTE PROCEDURE host_before_update();


CREATE OR REPLACE FUNCTION hostitel_before_insert()
RETURNS TRIGGER AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM uživatel WHERE uživatel_id = NEW.uživatel_uživatel_id AND uživatel_typ = 'host') THEN
    RAISE EXCEPTION 'Uživatel má typ "host" a proto nemůže být přiřazen jako "hostitel"';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER hostitel_before_insert BEFORE INSERT ON hostitel FOR EACH ROW
EXECUTE PROCEDURE hostitel_before_insert();


CREATE OR REPLACE FUNCTION hostitel_before_update()
RETURNS TRIGGER AS $$
BEGIN
--   IF EXISTS (SELECT 1 FROM User WHERE uživatel_id = NEW.hostitel_id AND uživatel_typ = 'host') THEN
  IF EXISTS (SELECT 1 FROM uživatel WHERE uživatel_id = NEW.uživatel_uživatel_id AND uživatel_typ = 'host') THEN
    RAISE EXCEPTION 'Uživatel má typ "host" a proto nemůže být přiřazen jako "hostitel"';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE TRIGGER hostitel_before_update BEFORE UPDATE ON hostitel FOR EACH ROW
EXECUTE PROCEDURE hostitel_before_update();



-- Utils
SELECT table_name 
FROM information_schema.tables 
WHERE table_schema = 'public';